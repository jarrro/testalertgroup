import './style.scss'
import axios from 'axios'


function addMemberFields(user = {name: '', phone: ''}) {
    let arrayRows = document.getElementsByClassName('array-rows')
    if(arrayRows.length > 1) {
        removeElement.style.display = 'block'
    } else {
        removeElement.style.display = 'none'
    }
    let html = `<div class="form-row array-rows">
                    <div class="col">
                        <input type="text" class="form-control" value="${user.name}">
                    </div>
                    <div class="col">
                        <input type="text" class="form-control" placeholder="+7 000 000 0000" value="${user.phone}">
                    </div>                                   
                </div>`
    let row = document.getElementsByClassName('array-rows')
    let firstRow = row[row.length-1] || document.getElementById('row-1')
    firstRow.insertAdjacentHTML('afterend', html)
}

loadRandom.addEventListener('click', () => {
    generate()
})

function generate() {
    let rows = document.getElementsByClassName('array-rows')

    for (let row of rows) {
        row.remove()
    }

    for (let i = 0; i < getRandomInt(10); i++) {
        axios.get('https://randomuser.me/api/')
            .then((result) => {
                console.log(result)
                addMemberFields({name: result.data.results[0].name.first + result.data.results[0].name.last, phone: result.data.results[0].phone})
            })

    }
}

function generateMainData() {
    return { object: generateObject() }
}

function generateObject() {
    let part1 = ['Альфа', 'Бета', 'Гамма', 'Эпсилон', 'Дельта', 'Стигма', 'Йота', 'Лямбда'][getRandomInt(7)],
        part2 = getRandomInt(100),
        part3 = ['блок', 'строение', 'пункт', 'площадь', 'зона', 'корпус', 'дом', 'этаж'][getRandomInt(7)]
    return part1 + ' ' + part2 + ' ' + part3
}

function generateApart() {
    return 'Квартира №' + getRandomInt(1000)
}

function genereateConstr() {
    return getRandomWord() + getRandomWord() + ' - ' + getRandomInt(100)
}

function generateRandomArea() {
    return getRandomInt(100) + 'м2'
}

function getRandomWord() {
    let alphabet = ["А", "Б", "В", "Г", "Д", "Е", "Ё", "Ж", "З", "И", "Й", "К", "Л", "М", "Н", "О", "П", "Р", "С", "Т", "У", "Ф", "Х", "Ц", "Ч", "Ш", "Щ", "Ъ", "Ы", "Ь", "Э", "Ю", "Я"]

    return alphabet[getRandomInt(33)]
}

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}


addElement.addEventListener('click', () => {
    addMemberFields()
    let rows = document.getElementsByClassName('array-rows')
    if(rows.length > 0) {
        removeElement.style.display = 'block'
    }
});


removeElement.addEventListener('click', () => {
    let rows = document.getElementsByClassName('array-rows')
   rows[rows.length-1].remove()
    if(rows.length < 2) {
        removeElement.style.display = 'none'
    }
});

function updateHtml(data = { object: 'Альфа 1 блок', apart: 'Квартира: № 78', construction: 'АЫ-26', fullArea: '60м2', room1: '60м2', kitchen: '60м2', balcony: '60м2', room2: '60м2', corridor: '60м2', room2a: '60м2', bathroom: '60м2'}) {
    let html2 = `
<div id="pattern">
   <div class="form-row">
        <div class="col">
            <span>Объект: <span>${generateObject()}</span></span>
        </div>
        <div class="col">
            <span>Квартира: <span>${generateApart()}</span></span>
        </div>
        <div class="col">
            <span>Строительные оси: <span>${genereateConstr()}</span></span>
        </div>
    </div>
    <div class="form-row">
        <div class="col">
            <span id="spoilerOn" class="show">Площадь:.........${generateRandomArea()}</span>
        </div>
    </div>
    <div class="form-row">
        <div class="col show" id="table">
            <table class="table">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Жилая</th>
                        <th scope="col">Нежилая</th>
                        <th scope="col">Площадь с пониж. кооф.</th>
                    </tr>
                </thead>                
            </table>
        </div>
    </div>
  </div>
`,
        tbody = `
        <tbody>
            <tr>
                <th>
                    Комната 1......... <span>N</span>
                </th>
                <th>
                    Кухня......... <span>N</span>
                </th>
                <th>
                    Балкон......... <span>N</span>
                </th>
            </tr>
            <tr>
                <th>
                    Комната 2......... <span>N</span>
                </th>
                <th>
                    Коридор......... <span>N</span>
                </th>    
                <th></th>                    
            </tr>
            <tr>
                <th>
                    Комната 2......... <span>N</span>
                </th>
                <th>
                    Санузел......... <span>N</span>
                </th>
                <th></th>
            </tr>
        </tbody>
`

    let secondRow = document.getElementById('row-2')
    secondRow.insertAdjacentHTML('afterend', html2)

    let thead = document.getElementsByTagName('thead')[0]
    thead.insertAdjacentHTML('afterend', tbody)
}

document.addEventListener('DOMContentLoaded', () => {
    updateHtml()
    addMemberFields()

    spoilerOn.addEventListener('click', () => {
        spoilerOn.className == 'show' ? spoilerOn.className = 'hide' : spoilerOn.className = 'show';

        if(table.classList.contains('show')) {
            table.classList.remove('show');
            table.classList.add('hide')
        } else {
            table.classList.remove('hide');
            table.classList.add('show')
        }
    });

    let data = [
        { object: 'Альфа 1 блок', apart: 'Квартира: № 78', construction: 'АЫ-26', fullArea: '60м2', room1: '60м2', kitchen: '60м2', balcony: '60м2', room2: '60м2', corridor: '60м2', room2a: '60м2', bathroom: '60м2'},
        { object: 'Бета 2 блок', apart: 'Квартира: № 79', construction: 'БЬ-27', fullArea: '78м2', room1: '85м2', kitchen: '33м2', balcony: '53м2', room2: '55м2', corridor: '59м2', room2a: '25м2', bathroom: '58м2'},
        { object: 'Гамма 3 блок', apart: 'Квартира: № 80', construction: 'ВЭ-28', fullArea: '64м2', room1: '96м2', kitchen: '44м2', balcony: '58м2', room2: '78м2', corridor: '67м2', room2a: '67м2', bathroom: '67м2'},
        { object: 'Дельта 4 блок', apart: 'Квартира: № 90', construction: 'ГЮ-29', fullArea: '25м2', room1: '107м2', kitchen: '66м2', balcony: '65м2', room2: '25м2', corridor: '25м2', room2a: '58м2', bathroom: '13м2'},
        { object: 'Эпсилон 5 блок', apart: 'Квартира: № 91', construction: 'ДЯ-30', fullArea: '38м2', room1: '55м2', kitchen: '77м2', balcony: '70м2', room2: '34м2', corridor: '67м2', room2a: '98м2', bathroom: '35м2'}
    ]

    select.addEventListener('change', (event) => {
        pattern.remove()
        updateHtml(data[event.target.value-1])
    })


});









// document.getElementsByClassName('remove-row').foreach(i of items).addEventListener('click', (event) => {
//     console.log(event)
// })